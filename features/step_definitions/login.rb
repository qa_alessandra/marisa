Dado('que estou na pagina de chat da Marisa') do
    visit 'https://marisa.virtualinteractions.com.br/pt_BR/avi.html?id=121&source=1&target=1&channel=1&licenseId=VI-88888846-1&ms=1588255788209#forward'
end
  
Quando('informar as credenciais {string}, {string}, {string},{string},{string}') do |nome, cpf, email, cartão, telefone|
    find('#vinter-loginForm-nome').set nome
    find('#vinter-loginForm-cpf').set cpf
    find('#vinter-loginForm-email').set email
    find(:select, 'vinter-loginForm-assunto').find("option[value='#{1}']", exact: true).select_option
    sleep 3
    find('#vinter-loginForm-telefone').set telefone
    sleep 3
end
  
Quando('clico em {string}') do |string|
    find('#vinter-btnSend').click
    sleep 3 
    
end
  
Entao('Devo inciar conversa com Assistente Virtual') do
    find('#vinter-pergunta').click
end

