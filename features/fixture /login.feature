#language:pt

Funcionalidade: Login no chat do site da Marisa

@login
Cenario: Login com dados validos 

Dado que estou na pagina de chat da Marisa   
Quando informar as credenciais "Gustavo Godoy", "318.380.507-36", "gustavo@teste.com.br","Cartão/Loja Fisica","11-951045859"
E clico em "Entrar no Chat"
Entao Devo inciar conversa com Assistente Virtual 